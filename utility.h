#pragma once
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// return how many times a word presents in a string.
size_t get_word_counter(const char *input, char *word) {
  size_t i, chars_counter = 0;
  size_t word_len = strlen(word);

  // Counting the number of times old word
  // occur in the string
  for (i = 0; input[i] != '\0'; i++) {
    if (strstr(&input[i], word) == &input[i]) {
      chars_counter++;

      // Jumping to index after the old word.
      i += word_len - 1;
    }
  }

  return chars_counter;
}

// return how many times '&', '<', '>' present in a string. e.g: te<st&, return 2
size_t get_found_counter(const char *input) {
  size_t my_chars_counter;

  my_chars_counter = get_word_counter(input, "<") + get_word_counter(input, "<") + get_word_counter(input, "<");

  return my_chars_counter;
}


// & -> &amp;
// < -> &lt;
// > -> &gt;
char *replace(char *input) {
  
  if (strlen(input) == 0) { /*Empty string*/
    char* ret = (char *)malloc(sizeof(char));
    ret[0] = '\0';
    return ret;
  }

  size_t and_counter = get_word_counter(input, "&");
  size_t less_than_counter = get_word_counter(input, "<");
  size_t greater_than_counter = get_word_counter(input, ">");
  size_t found_all_to_replace = and_counter + less_than_counter + greater_than_counter;
  size_t to_malloc = strlen(input) - found_all_to_replace + (and_counter * 5) + (less_than_counter * 4) + (greater_than_counter * 4) + 1;
  
  printf("input length %lu\n", strlen(input));
  printf("found total to replace %zu\n", get_found_counter((char*)input));
  printf("to_malloc, 1 for string null: %zu\n", to_malloc);

  char *str = (char *)malloc(sizeof(char) * (to_malloc));
  str[0] = '\0'; //init as a string
  
  if( str == NULL ) /*Check for failure. */
  {
      puts( "Can't allocate memory!" );
      exit( 0 );
  }
  
  size_t i;  // for the original input
  size_t j = 0;  // for str (where we copy the chars to)

  for (i = 0; input[i] ; i++) {
    if (!(input[i] == '&' || input[i] == '<' || input[i] == '>')) {
      strncat(str, &input[i], 1); // append a char to string
      j++;
    } else {
      if (input[i] == '&') {
        strcat(str, "&amp;"); // append (concatenates) string to string
        j += 5;
      } else if (input[i] == '<') {
        strcat(str, "&lt;");
        j += 4;
      } else if (input[i] == '>') {
        strcat(str, "&gt;");
        j += 4;
      }
    }
  }
  str[to_malloc - 1] = '\0'; // Set last byte of allocated string to '\0'

  printf("After replace: %s\n", str);

  return str;
}
